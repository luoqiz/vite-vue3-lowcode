import type { VisualEditorComponent } from '@/visual-editor/visual-editor.utils';
import { createEditorInputProp, createEditorSelectProp } from '@/visual-editor/visual-editor.props';
import { useGlobalProperties } from '@/hooks/useGlobalProperties';

export default {
  key: 'radiusTangle',
  moduleName: 'baseWidgets',
  label: '圆角矩形',
  preview: () => (
    <div style="border:1px solid red; border-radius: 10px 100px 60px 60px;panding:16px">
      圆角矩形
    </div>
  ),
  render: ({ props, block, styles }) => {
    const { registerRef } = useGlobalProperties();

    return () => (
      <div style={styles}>
        <div
          ref={(el) => registerRef(el, block._vid)}
          style="border:1px solid red; border-radius: 10px 100px 60px 60px;panding:16px"
        >
          {props.text}
        </div>
      </div>
    );
  },
  resize: {
    height: true,
    width: true,
  },
  events: [
    { label: '点击按钮，且按钮状态不为加载或禁用时触发', value: 'click' },
    { label: '开始触摸按钮时触发', value: 'touchstart' },
  ],
  props: {
    text: createEditorInputProp({ label: '按钮文字', defaultValue: '按钮' }),
    size: createEditorSelectProp({
      label: '按钮尺寸',
      options: [
        {
          label: '大型',
          value: 'large',
        },
        {
          label: '普通',
          value: 'normal',
        },
        {
          label: '小型',
          value: 'small',
        },
        {
          label: '迷你',
          value: 'mini',
        },
      ],
      defaultValue: 'normal',
    }),
    color: createEditorInputProp({
      label: '按钮颜色',
      tips: '按钮颜色，支持传入 linear-gradient 渐变色',
    }),
  },
} as VisualEditorComponent;
