/*
 * @Author: 卜启缘
 * @Date: 2021-05-04 05:36:58
 * @LastEditTime: 2021-07-14 10:31:10
 * @LastEditors: 卜启缘
 * @Description: 表单项类型 - 输入框
 * @FilePath: \vite-vue3-lowcode\src\packages\base-widgets\input\index.tsx
 */
import { createFieldProps } from './createFieldProps';
import type { VisualEditorComponent } from '@/visual-editor/visual-editor.utils';
import { useGlobalProperties } from '@/hooks/useGlobalProperties';

export default {
  key: 'nine-grid',
  moduleName: 'h5Components',
  label: '布局组件 - 九宫格',
  preview: () => (
    <div style="width:50%; aspect-ratio : 1 / 1; border:1px solid black;">
      <ul style="padding: 0; margin: 0; width: 100%; height: 100%; display: grid; grid-template-columns: 30% 30% 30%; grid-template-rows: 30% 30% 30%; grid-gap: 5%; justify-items: center;  align-items: center; text-align: center;">
        <li>1</li>
        <li>2</li>
        <li>3</li>
        <li>4</li>
        <li>5</li>
        <li>6</li>
        <li>7</li>
        <li>8</li>
        <li>9</li>
      </ul>
    </div>
  ),
  render: ({ styles, block, props }) => {
    const { registerRef } = useGlobalProperties();

    return () => (
      <div
        ref={(el) => registerRef(el, block._vid)}
        style="width:90%; aspect-ratio : 1 / 1; border:1px solid black;margin: auto;"
      >
        <ul style="padding: 0; margin: 0; width: 100%; height: 100%; display: grid; grid-template-columns: 30% 30% 30%; grid-template-rows: 30% 30% 30%; grid-gap: 5%; justify-items: center;  align-items: center; text-align: center;">
          <li style={styles}>{props.label}</li>
          <li style={styles}>2</li>
          <li style={styles}>3</li>
          <li style={styles}>4</li>
          <li style={styles}>5</li>
          <li style={styles}>6</li>
          <li style={styles}>7</li>
          <li style={styles}>8</li>
          <li style={styles}>9</li>
        </ul>
      </div>
    );
  },
  props: createFieldProps(),
} as VisualEditorComponent;
