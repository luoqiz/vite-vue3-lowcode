import { createEditorSwitchProp } from '@/visual-editor/visual-editor.props';

/**
 * @name: createFieldProps
 * @author: 卜启缘
 * @date: 2021/5/4 13:57
 * @description：createFieldProps
 * @update: 2021/5/4 13:57
 */
export const createFieldProps = () => ({
  autosize: createEditorSwitchProp({
    label: '自适应内容高度',
    defaultValue: false,
    tips: '是否自适应内容高度，只对 textarea 有效，可传入对象,如 { maxHeight: 100, minHeight: 50 }，单位为px',
  }),
  border: createEditorSwitchProp({ label: '是否显示内边框', defaultValue: true }),
});
