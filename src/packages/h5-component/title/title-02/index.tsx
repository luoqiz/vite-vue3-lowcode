/*
 * @Author: 卜启缘
 * @Date: 2021-05-04 05:36:58
 * @LastEditTime: 2021-07-14 10:31:10
 * @LastEditors: 卜启缘
 * @Description: 表单项类型 - 输入框
 * @FilePath: \vite-vue3-lowcode\src\packages\base-widgets\input\index.tsx
 */
import type { VisualEditorComponent } from '@/visual-editor/visual-editor.utils';
import { useGlobalProperties } from '@/hooks/useGlobalProperties';
import {
  createEditorColorProp,
  createEditorInputNumberProp,
  createEditorInputProp,
  createEditorSelectProp,
} from '@/visual-editor/visual-editor.props';
import { fontArr } from '@/packages/base-widgets/text/fontArr';

export default {
  key: 'h5-title-02',
  moduleName: 'h5Components',
  label: '标题 - title-02',
  preview: () => <div>测试文字</div>,
  render: ({ styles, block, props }) => {
    const { registerRef } = useGlobalProperties();

    return () => (
      <div
        ref={(el) => registerRef(el, block._vid)}
        style={{
          color: props.color,
          fontSize: `${parseFloat(props.size)}px`,
          fontFamily: props.font,
          ...styles,
        }}
      >
        {props.text || '默认文本'}
      </div>
    );
  },
  events: [
    { label: '输入框内容变化时触发', value: 'update:model-value' },
    { label: '输入框获得焦点时触发', value: 'focus' },
    { label: '输入框失去焦点时触发', value: 'blur' },
    { label: '点击清除按钮时触发', value: 'clear' },
    { label: '点击组件时触发', value: 'click' },
    { label: '点击输入区域时触发', value: 'click-input' },
    { label: '点击左侧图标时触发', value: 'click-left-icon' },
    { label: '点击右侧图标时触发', value: 'click-right-icon' },
  ],
  props: {
    text: createEditorInputProp({ label: '显示文本' }),
    font: createEditorSelectProp({ label: '字体设置', options: [...fontArr] }),
    color: createEditorColorProp({ label: '字体颜色' }),
    size: createEditorInputNumberProp({
      label: '字体大小',
      defaultValue: 16,
    }),
  },
  resize: {
    width: true,
  },
  model: {
    default: '绑定字段',
  },
} as VisualEditorComponent;
